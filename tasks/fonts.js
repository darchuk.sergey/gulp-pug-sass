import gulp from 'gulp';
import ttf2woff from 'gulp-ttf2woff';
import ttf2woff2 from 'gulp-ttf2woff2'
import path from 'node:path';
import { paths } from './config.js';


const fontsToWoff = () => {
    return gulp.src(path.resolve(paths.src.fonts) + '/*.ttf')
        .pipe(ttf2woff())
        .pipe(gulp.dest(path.resolve(paths.dev.fonts)));
}

const fontsToWoff2 = () => {
    return gulp.src(path.resolve(paths.src.fonts) + '/*.ttf')
        .pipe(ttf2woff2())
        .pipe(gulp.dest(path.resolve(paths.dev.fonts)));
}

const copyTtfFontsDev= () => {
    return gulp.src(path.resolve(paths.src.fonts) + '/*.ttf')
        .pipe(gulp.dest(path.resolve(paths.dev.fonts)));
}

const copyFontsBuild= () => {
    return gulp.src(path.resolve(paths.dev.fonts) + '/*.+(ttf|woff|woff2)')
        .pipe(gulp.dest(path.resolve(paths.build.fonts)));
}

const fontsDev = gulp.series(gulp.parallel(fontsToWoff, fontsToWoff2), copyTtfFontsDev);
const fontsBuild = copyFontsBuild;

export { fontsDev, fontsBuild }
