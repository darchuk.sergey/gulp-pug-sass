import gulp from 'gulp';
import pug from 'gulp-pug';
import notify from 'gulp-notify';
import plumber from 'gulp-plumber';
import path from 'node:path';
import { paths } from './config.js';
import { bs } from './server.js'

const compilePug = () => {
    return gulp.src(path.resolve(paths.src.html) + '/*.pug')
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: "Pug",
                message: error.message
            }))
        }))
        .pipe(pug({
            basedir: path.resolve(paths.src.html),
            doctype: "html",
            pretty: '    '
        }))
        .pipe(gulp.dest(path.resolve(paths.dev.base)))
        .pipe(bs.stream());
}

const htmlBuild = () => {
    return gulp.src(path.resolve(paths.dev.base) + '/*.html')
        .pipe(gulp.dest(path.resolve(paths.build.base)))
}

export { compilePug as htmlDev, htmlBuild }
