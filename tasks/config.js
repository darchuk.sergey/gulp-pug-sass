const paths = {
    src: {
        base: "./src",
        styles: "./src/styles",
        js: "./src/js",
        html: "./src/html",
        images: "./src/images",
        fonts: "./src/fonts"
    },
    dev: {
        base: "./dev",
        styles: "./dev/css",
        js: "./dev/js",
        images: "./dev/images",
        fonts: "./dev/fonts"
    },
    build: {
        base: "./build",
        styles: "./build/css",
        js: "./build/js",
        images: "./build/images",
        fonts: "./build/fonts"
    }
}

export { paths }