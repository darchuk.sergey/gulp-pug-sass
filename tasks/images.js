import gulp from 'gulp';
import changed from 'gulp-changed';
import imagemin, {gifsicle, mozjpeg, optipng, svgo} from 'gulp-imagemin';
import webp from 'gulp-webp';
import path from 'node:path';
import { paths } from './config.js';
import { bs } from './server.js';



const  convertImagesToWebp = () => {
    return gulp.src(path.resolve(paths.src.images) + '/**/*.+(png|jpg|jpeg)')
        .pipe(webp({quality: 50}))
        .pipe(gulp.dest(path.resolve(paths.dev.images)));
}

const compressImages = () => {
    return gulp.src(path.resolve(paths.src.images) + '/**/*.+(png|jpg|jpeg|gif|svg|ico)')
        .pipe(changed(path.resolve(paths.dev.images)))
        .pipe(imagemin([
            gifsicle({interlaced: true}),
            mozjpeg({quality: 75, progressive: true}),
            optipng({optimizationLevel: 5}),
            svgo({
                plugins: [
                    {
                        name: 'removeViewBox',
                        active: true
                    },
                    {
                        name: 'cleanupIDs',
                        active: false
                    }
                ]
            })
        ]))
        .pipe(gulp.dest(path.resolve(paths.dev.images)));
}

const copyImagesToBuild = () => {
    return gulp.src(path.resolve(paths.dev.images) + '/**/*.+(png|jpg|jpeg|gif|svg|ico|webp)')
        .pipe(gulp.dest(path.resolve(paths.build.images)))
        .pipe(bs.stream());
}

const imagesDev = gulp.parallel(convertImagesToWebp, compressImages);
const imagesBuild = copyImagesToBuild;

export { imagesDev, imagesBuild }
