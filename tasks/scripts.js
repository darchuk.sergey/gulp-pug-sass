import gulp from 'gulp';
import path from 'node:path';
import concat from 'gulp-concat';
import babel from 'gulp-babel';
import uglify from 'gulp-uglify';
import { paths } from './config.js';
import { bs } from './server.js';

const scriptsList = [
    path.resolve('./node_modules/jquery/dist', 'jquery.js'),
    path.resolve(paths.src.js, './common') + '/*.js',
    path.resolve(paths.src.js, './plugins') + '/*.js',
    path.resolve(paths.src.js) + '/*.js',
]

const scriptsDev = () => {
    return gulp.src(scriptsList)
        .pipe(babel({
        	presets: ['@babel/preset-env']
        }))
        .pipe(concat("app.js"))
        .pipe(gulp.dest(path.resolve(paths.dev.js)))
        .pipe(bs.stream());
}

const scriptsBuild = () => {
    return gulp.src(path.resolve(paths.dev.js) + '/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(path.resolve(paths.build.js)));
}

export { scriptsDev, scriptsBuild }