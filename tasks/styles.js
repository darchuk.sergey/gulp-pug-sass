import gulp from 'gulp';
import sourcemaps from 'gulp-sourcemaps';
import gulpSass from 'gulp-sass';
import dartSass from 'sass';
import autoPrefixer from 'gulp-autoprefixer';
import csso from 'gulp-csso';
import gcmq from 'gulp-group-css-media-queries';
import path from 'node:path';
import concat from 'gulp-concat';
import notify from 'gulp-notify';
import plumber from 'gulp-plumber';
import { paths } from './config.js';
import { bs } from './server.js';

const sass = gulpSass(dartSass);
const stylesList = [
    path.resolve(paths.src.styles, './common') + '/*.+(css|scss)', 
    path.resolve(paths.src.styles, './plugins') + '/*.+(css|scss)',
    path.resolve(paths.src.styles, 'main.scss')
];

const stylesDev = () => {
    return gulp.src(stylesList)
        .pipe(sourcemaps.init())
        .pipe(plumber({
            errorHandler: notify.onError(error => ({
                title: "CSS",
                message: error.message
            }))
        }))
        .pipe(sass())
        .pipe(autoPrefixer({
            cascade: false
        }))
        .pipe(gcmq())
        .pipe(concat('styles.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.resolve(paths.dev.styles)))
        .pipe(bs.stream());
}

const stylesBuild = () => {
    return gulp.src(path.resolve(paths.dev.styles) + '/*.css')
        .pipe(csso())
        .pipe(gulp.dest(path.resolve(paths.build.styles)));
}

export { stylesDev, stylesBuild }
