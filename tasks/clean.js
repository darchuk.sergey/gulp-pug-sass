import del from 'del';
import path from 'node:path';
import { paths } from './config.js';

const cleanDev = () => {
    //console.log(path.resolve(paths.dev.base));
    return del(path.resolve(paths.dev.base));
}

const cleanBuild = () => {
    return del(path.resolve(paths.build.base));
}

export { cleanBuild, cleanDev }