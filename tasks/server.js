import browserSync from 'browser-sync';
import path from 'node:path';
import { paths } from './config.js';

const bs = browserSync.create();

const serveDev = (done) => {
    bs.init({
        server: {
            baseDir: path.resolve(paths.dev.base)
        }
    });
    done();
}

const serveBuild = (done) => {
    bs.init({
        server: {
            baseDir: path.resolve(paths.build.base)
        }
    });
    done();
}

const reload = (done) => {
    bs.reload();
    done();
}

export { bs, serveDev, serveBuild, reload }