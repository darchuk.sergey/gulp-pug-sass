import gulp from 'gulp';
import { paths } from './tasks/config.js';
import { cleanDev, cleanBuild } from './tasks/clean.js';
import { htmlBuild, htmlDev } from './tasks/html.js';
import { stylesBuild, stylesDev } from './tasks/styles.js';
import { serveBuild, serveDev } from './tasks/server.js';
import { fontsBuild, fontsDev } from './tasks/fonts.js';
import { imagesBuild, imagesDev } from './tasks/images.js'
import { scriptsBuild, scriptsDev } from './tasks/scripts.js';
 

const watch = () => {
    gulp.watch(paths.src.js + "/**/*.js", scriptsDev);
    gulp.watch(paths.src.styles + "/**/*.+(scss|css)", stylesDev);
    gulp.watch(paths.src.html + "/**/*.pug", htmlDev);
}

export const build = gulp.series(cleanBuild, gulp.parallel(htmlBuild, stylesBuild, fontsBuild, imagesBuild, scriptsBuild), serveBuild);

export default gulp.series(cleanDev, gulp.parallel(htmlDev, stylesDev, fontsDev, imagesDev, scriptsDev), serveDev, watch);